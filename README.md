# Aeriandi Freeswitch

### Building a new branch

#### Set up git
For convenience, set up two remotes - one for freeswitch.org stash and one for Aeriandi stash. I've called these 'fs' for the Freeswitch repo and 'stash' for our own repo. You can see your remotes with `git remote -v`

```
fs	https://freeswitch.org/stash/scm/fs/freeswitch.git (fetch)
fs	https://freeswitch.org/stash/scm/fs/freeswitch.git (push)
stash	https://you@stash.liquid-contact.com/scm/tel/freeswitch.git (fetch)
stash	https://you@stash.liquid-contact.com/scm/tel/freeswitch.git (push)
```

#### Get the latest changes

Retrieve the latest changes from Freeswitch.org

```
git fetch fs
```

#### Create a new branch for the build

Find the latest build number and increment the major or minor number as appropriate to obtain the new build number, then create a branch (from master or the appropriate bugfix branch) with that name. Eg, if the new build is version 21.3 and you're building from master, type:

```
git checkout -b 21.3 fs/master
```

You now have a branch with the vanilla source of Freeswitch. You will want to add the Aeriandi patches before doing the build...

#### Add Aeriandi patches

Do a `git log` of the previous build and note down the SHA hashes of the most recent commits up until you hit a commit whose author is not an Aeriandi employee. These are our patches that will need to be applied to our new branch.

```
git log 21.2
```

For each of these patches you need to `git cherry-pick` the commit into your new branch. You should do them in order to avoid conflicts.

Eg, for each one:

```
git cherry-pick 01234567890abcdef01234567890abcdef
```

#### Start the build

Pushing the branch to Aeriandi stash will trigger a new build on TeamCity.

```
git push -u stash 21.3
```
